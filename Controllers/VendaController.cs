using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.Context;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;
        public VendaController(VendaContext context)
        {
            _context = context;
        }
        [HttpPost("Registrar Venda")]
        public IActionResult RegistrarVenda(Venda venda)
        {
            if (venda.Itens == null) return BadRequest();
            var vendedorBanco = _context.Vendedores.Find(venda.VendedorId);
            if (vendedorBanco == null) return NotFound("Vendedor não existente");

            foreach (var item in venda.Itens)
                _context.Itens.Add(item);
            venda.Status = EnumStatusVenda.AguardandoPagamento;
            _context.Vendas.Add(venda);
            _context.SaveChanges();
            return Ok(venda);
        }

        [HttpGet("Buscar por Id")]
        public IActionResult BuscarPorId(int id)
        {
            var venda = _context.Vendas.Find(id);
            if (venda == null) return NotFound();
            venda.Itens = _context.Itens.Where(x => x.VendaId == venda.Id).ToList();
            return Ok(venda);
        }
        [HttpPut("Atualizar status {id}")]
        public IActionResult AtualizarVenda(int id, EnumStatusVenda status)
        {
            var vendaBanco = _context.Vendas.Find(id);
            if (vendaBanco == null) return NotFound();
            switch(vendaBanco.Status)
            {
                case EnumStatusVenda.AguardandoPagamento:
                    if (status != EnumStatusVenda.PagamentoAprovado && status != EnumStatusVenda.Cancelada)
                        return BadRequest();
                    vendaBanco.Status = status;
                    break;
                case EnumStatusVenda.PagamentoAprovado:
                    if (status != EnumStatusVenda.EnviadoParaTransportadora && status != EnumStatusVenda.Cancelada)
                        return BadRequest();
                    vendaBanco.Status = status;
                    break;
                case EnumStatusVenda.EnviadoParaTransportadora:
                    if (status != EnumStatusVenda.Entregue)
                        return BadRequest();
                    vendaBanco.Status = status;
                    break;
                default:
                    return BadRequest();
            }
            _context.Vendas.Update(vendaBanco);
            _context.SaveChanges();
            return Ok(vendaBanco);
        }
    }
}